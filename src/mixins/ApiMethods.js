import russian_names from '@/data/russian_names.json'
import russian_surnames from '@/data/russian_surnames.json'

export default {
    data: () => ({
        loading: true
    }),
    methods: {
        getDataNamesFromAPI(query) {
            return new Promise((resolve, reject) => {
                const items = this.dataNamesFromDB().filter((e) => {
                    if(e.Name.indexOf(query) != -1) {
                        return true;
                    }
                })
                setTimeout(() => {
                    this.loading = false;
                    resolve({
                        items
                    })
                }, 1000)
            })
        },
        getDataSureNamesFromAPI(query) {
            return new Promise((resolve, reject) => {
                const items = this.dataSurenamesFromDB().filter((e) => {
                    if(e.Surname.indexOf(query) != -1) {
                        return true;
                    }
                })
                setTimeout(() => {
                    this.loading = false
                    resolve({
                        items
                    })
                }, 1000)
            })
        },
        dataNamesFromDB() {
            return russian_names
        },
        dataSurenamesFromDB() {
            return russian_surnames
        }
    }
}
